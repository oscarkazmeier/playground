//FS MODULE
// use async-await OR promises as alternative to callback-hell
console.log('start')
//(a-syncronous)
const {readFile, writeFile} = require('fs')    //executed as readFileSync()

readFile('./content/first.txt','utf8',(err,result)=>{
    if(err){
        return
    }
    console.log(result)
    const first = result;
    readFile('./content/second.txt','utf8',(err,result)=>{
        if(err){
            return
        }
        console.log(result)
        const second = result
        writeFile(
            './content/result-async.txt',
            `Here is the result: ${first}, ${second}`,
            (err,result)=>{
                if(err){
                    console.log('failed on create!')
                    return
                }
                // console.log(`This is the final result: ${result}`) //will be ..undefined.. (ok)
                console.log('done with the task of writing into new file')
            }
        )
    })
})
console.log('starting the next task')