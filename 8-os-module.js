//OS MODULE
const os = require('os')

//info about curretn user
const user = os.userInfo()
console.log(user)

//method returns system Uptime in s
console.log(`System updatime is : ${os.uptime()} seconds`)

const currentOS = {
    name: os.type(),
    release:os.release(),
    totalMem: os.totalmem(),
    freeMem: os.freemem()
}
console.log(currentOS);