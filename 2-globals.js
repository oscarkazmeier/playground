// GLOBALS
// __dirname    - path to current dir
// __filename   - filename
// require      - function to use modules (CommonJS)
// module       - info about current module (file) [built in Modules i.e.: OS, PATH, FS, HTTP]
// process      - info about env where the program is being executed

console.log("dirname:"+__dirname);
console.log("file name:"+__filename);
//setTimeout..similar to interval but just once
setInterval(() => {
    console.log('hello');
},1000)


