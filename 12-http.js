//HTTP MODULE

const http = require('http')
const server = http.createServer((req,res)=>{
    // console.log(req)
    if(req.url=== '/'){
        res.write('Welcome to the home page')
        res.end()
        // res.end('Welcome to the home page')
    }else if(req.url === '/about'){
        res.write('This is the about page')
        res.end()
    }else{
        res.end('<h1>Ops, nothing here</h1><br><a href="/">home</a>')
    }
})
server.listen(5000)
