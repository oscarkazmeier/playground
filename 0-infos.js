npmjs.com   //resources for nodeJS

//npm - global command, comes with nodeJS
// npm --version

//local dependency - use it only in this particular project
// npm i <packageName>

//global dependency - use it in any project// npm install -g <packageName>
// sudo npm install -g <packageName> (mac)

// package.json - manifest file (stores important info about our project/package)
// manual approach (create package.json in the root, create properties etc.)
// npm init -y (everything default)

// package start scripts
// "scripts": {
//     "start": "node app.js",   // npm start
//     "dev": "nodemon app.js",  // npm run dev
//     "test": "echo \"Error: no test specified\" && exit 1"
// },