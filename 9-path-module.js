//PATH MODULE
const path = require('path')

console.log(path.sep)

const filePath = path.join('/content','/subfolder','test.txt')
console.log('filePath :'+filePath)

const base = path.basename(filePath)
console.log('base : '+base)

// const absPath = path.resolve(__dirname, filePath) //wont work
const absPath = path.resolve(__dirname, 'content','subfolder', 'test.txt')
console.log('absPath: '+absPath)